import argparse
import os
import singer
import requests
import urllib.request
import json
import time
from uuid import uuid4
from datetime import datetime, timezone
from singer import utils

now = datetime.now(timezone.utc).isoformat()

LOGGER = singer.get_logger()

# Load schemas from schemas folder
def load_schemas():
    schemas = {}

    for filename in os.listdir(get_abs_path('schemas')):
        path = get_abs_path('schemas') + '/' + filename
        file_raw = filename.replace('.json', '')
        with open(path) as file:
            schemas[file_raw] = json.load(file)

    return schemas

def discover():
    raw_schemas = load_schemas()
    streams = []

    for schema_name, schema in raw_schemas.items():
        # TODO: populate any metadata and stream's key properties here..
        stream_metadata = []
        stream_key_properties = []

        # create and add catalog entry
        catalog_entry = {
            'stream': schema_name,
            'tap_stream_id': schema_name,
            'schema': schema,
            'metadata' : [],
            'key_properties': []
        }
        streams.append(catalog_entry)

    return {'streams': streams}

def get_abs_path(path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), path)

def load_schema(entity):
    return utils.load_json(get_abs_path(f"schemas/{entity}.json"))

def write_schemas():
    singer.write_schema("region", load_schema("region"), "id")
    singer.write_schema("entry", load_schema("entry"), "id")
    singer.write_schema("generationmix", load_schema("generationmix"), "id")

def create_region(resp):
    data = resp["data"]
    return {
      "id": data["regionid"],
      "dnoregion": data["dnoregion"],
      "shortname": data["shortname"]
    }

def create_entry(data, region_id):
    return {
      "id": str(uuid4()),
      "from": data["from"],
      "to": data["to"],
      "region_id": region_id,
      "forecast": data["intensity"]["forecast"],
      "index": data["intensity"]["index"]
    }

def create_generationmix(data, entry_id):
    return {
      "id": str(uuid4()),
      "fuel": data["fuel"],
      "perc": data["perc"],
      "entry_id": entry_id
    }

def get_data(gen):
    try:
        regionid = next(gen)
    except Exception as e:
        return

    carbon_api_url = f"https://api.carbonintensity.org.uk/regional/intensity/2018-05-15T12:00Z/2018-05-16T12:00Z/regionid/{regionid}"

    tries = 3
    while True:
        resp = requests.get(carbon_api_url)
        LOGGER.info(f"GET {carbon_api_url}")

        try:
            resp.raise_for_status()

            # Break out of loop if no exception was raised
            break
        except requests.HTTPError as e:
            LOGGER.info(f"Request failed: {e}")

            tries -= 1
            LOGGER.info(f"Tries remaining: {tries}")

            if tries == 0:
                LOGGER.info("Failed too many times, raising error")
                raise e

            LOGGER.info("Sleeping for 1 second and retrying...")
            time.sleep(1)

    resp = resp.json()
    region = create_region(resp)
    region_id = region["id"]
    singer.write_record("region", region)
    for data_entry in resp["data"]["data"]:
        entry = create_entry(data_entry, region_id)
        entry_id = entry["id"]
        singer.write_record("entry", entry)
        for generationmix_data in data_entry["generationmix"]:
          generationmix = create_generationmix(generationmix_data, entry_id)
          singer.write_record("generationmix", generationmix)

    LOGGER.info(f"Loaded region {region['shortname']}")

    singer.write_state({"region": region_id})
    get_data(gen)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", help="Config file", required=False)
    parser.add_argument("-s", "--state", help="State file", required=False)
    parser.add_argument("-p", "--properties", help="Property selections", required=False)
    parser.add_argument("--catalog", help="Catalog file", required=False)
    parser.add_argument("-d", "--discover", action='store_true', help="Do schema discovery", required=False)
    args = parser.parse_args()

    # If discover flag was passed, run discovery mode and dump output to stdout
    if args.discover:
        catalog = discover()
        print(json.dumps(catalog, indent=2))
    # Otherwise run in sync mode
    else:
      write_schemas()
      region_gen = (i for i in range(1, 18, 1))
      get_data(region_gen)
