# tap-carbon-intensity

Uses https://api.carbonintensity.org.uk/

## Getting Started

### Install
```bash
git clone git@gitlab.com:meltano/tap-carbon-intensity.git
cd tap-carbon-intensity
pip install .
```

### Use
```bash
tap-carbon-intensity
```

### For Developers

1. If you do not have a virtual environment setup yet for this folder, please run:

```bash
python -m venv venv
```

2. Activate your virtual environment:

```bash
source ./venv/bin/activate
```

3. Install the tap

```bash
pip install -e .
```

> Note: The `-e` flag marks the installation as editable, which tracks any changes that you may make

4. Now you can run the following command to run the tap:

```bash
tap-carbon-intensity
```
