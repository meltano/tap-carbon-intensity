from setuptools import setup

setup(
  name='tap-carbon-intensity',
  version='1.0',
  py_modules=['tap_carbon_intensity'],
  install_requires=[
    'requests',
    'singer-python',
  ],
  entry_points='''
    [console_scripts]
    tap-carbon-intensity=tap_carbon_intensity:main
  ''',
  packages=['tap_carbon_intensity'],
  package_dir={'tap_carbon_intensity': "tap_carbon_intensity"},
  package_data = {'tap_carbon_intensity': ["schemas/*.json", "LICENSE"]},
  include_package_data=True
)
